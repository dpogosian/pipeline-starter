#!/bin/bash

echo 'Spinning the failure week:'

if [[ $((1 + $RANDOM % 10)) -gt 7 ]]; then
    echo 'You lose!'
    exit 1
else
	echo 'You win!'
fi